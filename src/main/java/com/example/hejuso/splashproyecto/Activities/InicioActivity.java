package com.example.hejuso.splashproyecto.Activities;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;

import com.example.hejuso.splashproyecto.R;

import java.util.Timer;
import java.util.TimerTask;

public class InicioActivity extends AppCompatActivity {
    private long SPLASH_DELAY = 3000;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_inicio);
    }
    @Override
    protected void onStart() {
        super.onStart();

        //TimerTask
        TimerTask task = new TimerTask() {
            @Override public void run() {
                Intent mainIntent = new Intent(getApplicationContext(),
                        MainActivity.class);
                startActivity(mainIntent);
                //Destruimos esta activity para prevenir
                // que el usuario vuelva a este Activity presionando el boton
                // Atras.
                finish();
            }
        };
        Timer timer = new Timer();
        timer.schedule(task, SPLASH_DELAY);

        setContentView(R.layout.activity_inicio);
    }
}
