package com.example.hejuso.splashproyecto.Adapter;


import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentActivity;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;


import com.example.hejuso.splashproyecto.Fragment.JuegoFragment;
import com.example.hejuso.splashproyecto.Fragment.PefilFragment;
import com.example.hejuso.splashproyecto.Fragment.SelfieFragment;
import com.example.hejuso.splashproyecto.Fragment.UbicacionFragment;
import com.example.hejuso.splashproyecto.Modelo.DatosMenu;
import com.example.hejuso.splashproyecto.R;

import java.util.List;

/**
 * Created by hejuso on 06/12/2017.
 */

public class ListAdapter extends RecyclerView.Adapter<ListAdapter.ListViewHolder> {

    private List<DatosMenu> llistat;

        public class ListViewHolder extends RecyclerView.ViewHolder{

        public TextView titol;
        public ImageView imatge;

        public ListViewHolder(View v) {
            super(v);
            titol = itemView.findViewById(R.id.titol);
            imatge = itemView.findViewById(R.id.imatge);

            // Listener per al recycler view
            titol.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View vista) {
                    switch (getAdapterPosition()){
                        case 0:
                            AppCompatActivity activityPerfil = (AppCompatActivity) vista.getContext();
                            Fragment fragmentPerfil = new PefilFragment();
                            activityPerfil.getSupportFragmentManager().beginTransaction().replace(R.id.frameMenu_2, fragmentPerfil).addToBackStack(null).commit();

                            break;
                        case 1:
                            AppCompatActivity activityJuego = (AppCompatActivity) vista.getContext();
                            Fragment fragmentJuego = new JuegoFragment();
                            activityJuego.getSupportFragmentManager().beginTransaction().replace(R.id.frameMenu_2, fragmentJuego).addToBackStack(null).commit();

                            break;
                        case 2:
                            AppCompatActivity activitySelfie = (AppCompatActivity) vista.getContext();
                            Fragment fragmentSelfie = new SelfieFragment();
                            activitySelfie.getSupportFragmentManager().beginTransaction().replace(R.id.frameMenu_2, fragmentSelfie).addToBackStack(null).commit();
                            break;
                        case 3:
                            AppCompatActivity activityUbicacio = (AppCompatActivity) vista.getContext();
                            Fragment fragmentUbicacio = new UbicacionFragment();
                            activityUbicacio.getSupportFragmentManager().beginTransaction().replace(R.id.frameMenu_2, fragmentUbicacio).addToBackStack(null).commit();

                            Toast.makeText(vista.getContext(), "Ubicació",Toast.LENGTH_SHORT).show();
                            break;
                    }
                    Log.e("Test","Name clicked : "+getAdapterPosition());
                }
            });

        }



    }

    public ListAdapter(List<DatosMenu> llistat) {
        this.llistat = llistat;
    }

    @Override
    public ListViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View v = LayoutInflater.from(parent.getContext()).inflate(R.layout.llistat_recycler, parent, false);
        return new ListViewHolder(v);
    }

    @Override
    public void onBindViewHolder(ListViewHolder holder, int position) {
        // Ens guardem en una variable la id de la imatge  del Drawable que li hem pasat abans
        int id = llistat.get(position).getImatge();

        holder.titol.setText(llistat.get(position).getTitol());

        // Li fiquem la imatge
        holder.imatge.setImageResource(id);
    }

    @Override
    public int getItemCount () {
        int e = this.llistat.size();
        return e;
    }

}