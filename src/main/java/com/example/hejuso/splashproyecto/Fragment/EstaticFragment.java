package com.example.hejuso.splashproyecto.Fragment;

import android.content.Context;
import android.net.Uri;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.example.hejuso.splashproyecto.Adapter.ListAdapter;
import com.example.hejuso.splashproyecto.Modelo.DatosMenu;
import com.example.hejuso.splashproyecto.R;

import java.util.ArrayList;
import java.util.List;


public class EstaticFragment extends Fragment {
    RecyclerView rv;
    RecyclerView.LayoutManager rvLM;
    ListAdapter aRVclient;
    List llistat;

    private OnFragmentInteractionListener mListener;


    // TODO: Rename and change types and number of parameters
    public static EstaticFragment newInstance() {
        EstaticFragment fragment = new EstaticFragment();
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (getArguments() != null) {
        }

    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View vista = inflater.inflate(R.layout.fragment_estatic, container, false);

        // Creem array para el llistat
        llistat = new ArrayList();

        // I l'omplim amb 4 opcions (Li afegim també les imatges que volem implementarli)
        llistat.add(new DatosMenu("Perfil", R.drawable.user));
        llistat.add(new DatosMenu("Joc",R.drawable.playbutton));
        llistat.add(new DatosMenu("Selfie",R.drawable.list));
        llistat.add(new DatosMenu("Ubicació",R.drawable.info));
        // Obtenim una instància del RecyclerView
        rv = vista.findViewById(R.id.recycler_exemple);

        //Triem el LayoutManager que volem utilitzar i l'assignem a l'objecte recyclerView
        rvLM = new LinearLayoutManager(getActivity());
        rv.setLayoutManager(rvLM);

        // Creem l'adaptador que interactuarà amb les dades
        aRVclient = new ListAdapter(llistat);

        // Enllacem el RecyclerView amb l'adaptador
        rv.setAdapter(aRVclient);

        // Inflate the layout for this fragment
        return vista;
    }

    public void onButtonPressed(Uri uri) {
        if (mListener != null) {
            mListener.onFragmentInteraction(uri);
        }
    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        if (context instanceof OnFragmentInteractionListener) {
            mListener = (OnFragmentInteractionListener) context;
        } else {
            throw new RuntimeException(context.toString()
                    + " must implement OnFragmentInteractionListener");
        }
    }

    @Override
    public void onDetach() {
        super.onDetach();
        mListener = null;
    }

    /**
     * This interface must be implemented by activities that contain this
     * fragment to allow an interaction in this fragment to be communicated
     * to the activity and potentially other fragments contained in that
     * activity.
     * <p>
     * See the Android Training lesson <a href=
     * "http://developer.android.com/training/basics/fragments/communicating.html"
     * >Communicating with Other Fragments</a> for more information.
     */
    public interface OnFragmentInteractionListener {

        // TODO: Update argument type and name
        void onFragmentInteraction(Uri uri);
    }

}
